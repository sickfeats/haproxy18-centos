## HAProxy 

This repo builds haproxy 1.8

## run

This command runs haproxy in foreground.

```
docker run --rm -it --name my-running-haproxy \
    -p 13443:13443 \
    -e TZ=Europe/Vienna \
    -e DEBUG=true \
    -e STATS_PORT=1999 \
    -e STATS_USER=aaa \
    -e STATS_PASSWORD=bbb \
    -e SERVICE_TCP_PORT=13443 \
    -e SERVICE_NAME=test-haproxy \
    -e SERVICE_DEST_PORT=80 \
    -e SERVICE_DEST='www.haproxy.org' \
    me2digital/haproxy18
```