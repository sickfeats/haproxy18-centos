core.register_action("print-headers",{ "http-req" }, function(transaction)
    --[[
        transaction is of class TXN.
        TXN contains contains a property 'http' which is an instance
        of HAProxy HTTP class
    ]]

    local hdr = transaction.http:req_get_headers()

    for key,value in pairs(hdr) do
      local mystr = key
      mystr = mystr .. ": "
      for _ ,val2 in pairs(value) do
        mystr = mystr .. val2
      end
      core.Info(mystr)
    end

end)
